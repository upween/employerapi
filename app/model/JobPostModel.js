'user strict';
var sql = require('./db.js');

var JobPost = function(JobPost){
    this.p_JobId = JobPost.p_JobId;
    this.p_JobTitle = JobPost.p_JobTitle;
    this.p_JobDescription = JobPost.p_JobDescription;
    this.p_Keywords = JobPost.p_Keywords;
    this.p_WorkExpMin = JobPost.p_WorkExpMin;
    this.p_WorkExpMax = JobPost.p_WorkExpMax;
    this.p_AnnualCTCCur = JobPost.p_AnnualCTCCur;
    this.p_AnnualCTCMin = JobPost.p_AnnualCTCMin;
    this.p_AnnualCTCMax = JobPost.p_AnnualCTCMax;
    this.p_OtherSalDet = JobPost.p_OtherSalDet;
    this.p_NoOfVacancy = JobPost.p_NoOfVacancy;
    this.p_Location = JobPost.p_Location;
    this.p_Industry = JobPost.p_Industry;
    this.p_FunctionalArea = JobPost.p_FunctionalArea;
    this.p_EmploymentDetails = JobPost.p_EmploymentDetails;
    this.p_UGQualifications = JobPost.p_UGQualifications;
    this.p_PGQualifications = JobPost.p_PGQualifications;
    this.p_PHDQualification = JobPost.p_PHDQualification;
    this.p_ReponsesOn = JobPost.p_ReponsesOn;
    this.p_ReferenceCode = JobPost.p_ReferenceCode;
    this.p_Email = JobPost.p_Email;
    this.p_Name = JobPost.p_Name;
    this.p_About = JobPost.p_About;
    this.p_Website = JobPost.p_Website;
    this.p_ContactPerson = JobPost.p_ContactPerson;
    this.p_ContactNumber = JobPost.p_ContactNumber;
    this.p_Address = JobPost.p_Address;
    this.p_RefreshJob = JobPost.p_RefreshJob;
    this.p_IsActive = JobPost.p_IsActive;
};
JobPost.createJobPost = function createUser(newJobPost, result) {  
 
    sql.query("CALL InsUpd_Job_Posting('"+newJobPost.p_JobId+"','"+newJobPost.p_JobTitle+"','"+newJobPost.p_JobDescription+"','"+newJobPost.p_Keywords+"','"+newJobPost.p_WorkExpMin+"','"+newJobPost.p_WorkExpMax+"','"+newJobPost.p_AnnualCTCCur+"','"+newJobPost.p_AnnualCTCMin+"','"+newJobPost.p_AnnualCTCMax+"','"+newJobPost.p_OtherSalDet+"','"+newJobPost.p_NoOfVacancy+"','"+newJobPost.p_Location+"','"+newJobPost.p_Industry+"','"+newJobPost.p_FunctionalArea+"','"+newJobPost.p_EmploymentDetails+"','"+newJobPost.p_UGQualifications+"','"+newJobPost.p_PGQualifications+"','"+newJobPost.p_PHDQualification+"','"+newJobPost.p_ReponsesOn+"','"+newJobPost.p_ReferenceCode+"','"+newJobPost.p_Email+"','"+newJobPost.p_Name+"','"+newJobPost.p_About+"','"+newJobPost.p_Website+"','"+newJobPost.p_ContactPerson+"','"+newJobPost.p_ContactNumber+"','"+newJobPost.p_Address+"','"+newJobPost.p_RefreshJob+"','"+newJobPost.p_IsActive+"')", function (err, res) {      
        if(err) {
           
            result(err, null);
        }
        else{
            
            result(null, res);
        }
    });           
};


module.exports= JobPost;