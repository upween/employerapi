'user strict';
var sql = require('./db.js');

var FetchJob = function(fetchjob){
    this.p_JobId = fetchjob.p_JobId;
    this.p_JobTitle = fetchjob.p_JobTitle;
    this.p_JobDescription = fetchjob.pJobDescription;
    this.p_Keywords = fetchjob.p_Keywords;
    this.p_WorkExpMin = fetchjob.p_WorkExpMin;
    this.p_WorkExpMax = fetchjob.p_WorkExpMax;
    this.p_AnnualCTCCur = fetchjob.p_AnnualCTCCur;
    this.p_AnnualCTCMin = fetchjob.p_AnnualCTCMin;
    this.p_AnnualCTCMax = fetchjob.p_AnnualCTCMax;
    this.p_OtherSalDet = fetchjob.p_OtherSalDet;
    this.p_NoOfVacancy = fetchjob.p_NoOfVacancy;
    this.p_Location = fetchjob.p_Location;
    this.p_Industry = fetchjob.p_Industry;
    this.p_FunctionalArea = fetchjob.p_FunctionalArea;
    this.p_EmploymentDetails = fetchjob.p_EmploymentDetails;
    this.p_UGQualifications = fetchjob.p_UGQualifications;
    this.p_PHDQualification = fetchjob.p_PHDQualification;
    this.p_ReponsesOn = fetchjob.p_ReponsesOn;
    this.p_ReferenceCode = fetchjob.p_ReferenceCode;
    this.p_Email = fetchjob.p_Email;
    this.p_Name = fetchjob.p_Name;
    this.p_About = fetchjob.p_About;
    this.p_Website = fetchjob.p_Website;
    this.p_ContactPerson = fetchjob.p_ContactPerson;
    this.p_ContactNumber = fetchjob.p_ContactNumber;
    this.p_Address = fetchjob.p_Address;
    this.p_RefreshJob = fetchjob.p_RefreshJob;
    this.p_IsActive = fetchjob.p_IsActive;
    this.p_Flag = fetchjob.p_Flag;    
};

FetchJob.Fetch_Job = function createUser(fetch_job, result) {
    
    sql.query("CALL Fetch_Job("+fetch_job.p_Location+","+fetch_job.p_Flag+")", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
            
            result(null, res);
        }
    });   
};

module.exports = FetchJob;