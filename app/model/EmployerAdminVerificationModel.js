'user strict';
var sql = require('./db.js');

var EmployerAdminVerification = function(EmployerAdminVerification){
    this.p_RegistraionNumber = EmployerAdminVerification.p_RegistraionNumber;
    this.p_RegistrationMonth = EmployerAdminVerification.p_RegistrationMonth;
    this.p_RegistrationYear = EmployerAdminVerification.p_RegistrationYear;
    this.p_Emp_Name = EmployerAdminVerification.p_Emp_Name;
    this.p_Company_Type = EmployerAdminVerification.p_Company_Type;
    this.p_Emp_RegId = EmployerAdminVerification.p_Emp_RegId;
};

EmployerAdminVerification.getEmployerAdminVerification= function createUser(Fetch_Employer_AdminVerification, result) {
    
    sql.query("CALL Fetch_Employer_AdminVerification("+Fetch_Employer_AdminVerification.p_RegistraionNumber+","+Fetch_Employer_AdminVerification.p_RegistrationMonth+","+Fetch_Employer_AdminVerification.p_RegistrationYear+","+Fetch_Employer_AdminVerification.p_Emp_Name+","+Fetch_Employer_AdminVerification.p_Company_Type+")", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
            
            result(null, res);
        }
    });   
};

EmployerAdminVerification.getDeleteEmpDetail= function createUser(Delete_Emp_Detail, result) {
    
    sql.query("CALL Delete_Emp_Detail("+Delete_Emp_Detail.p_Emp_RegId+")", function (err, res) {             
        if(err) {
            
            result(err, null);
        }
        else{
            
            result(null, res);
        }
    });   
};




module.exports= EmployerAdminVerification;