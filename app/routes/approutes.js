'use strict';
import TokenValidator from '../../middleware/TokenValidator'
module.exports = function (app) {
  var categ = require('../controller/appController');
  var employerregistrations = require('../controller/employerregistrationsController');
  var validateemplogin = require('../controller/validateemployerloginController');
  var JobPost = require('../controller/JobPostController');
  var FetchJob = require('../controller/FetchJobController');
  var EmployerAdminVerification = require('../controller/EmployerAdminVerificationController');
  










  // todoList Routes
  app.use(TokenValidator)
  app.route('/category')
    //.get(categ.list_all_category)
    .post(categ.create_a_category);
  app.route('/secured/category/:p_categoryid/:p_IpAddress/:p_IsActive/:p_UserId')
    .get(categ.read_a_category);
  app.route('/category/:p_categoryid/:p_IpAddress/:p_IsActive/:p_UserId')
    .get(categ.read_a_category);
  //.put(categ.update_a_category)
  //.delete(categ.delete_a_category);

  //employer registrations Route Begin
  app.route('/employerregistrations')
    .post(employerregistrations.create_a_EmployerRegistration);
  app.route('/employerregistrations/:p_EmployerId/:p_IpAddress/:p_UserId')
    .get(employerregistrations.read_a_EmployerRegistration);
  //employer registrations Route End
  
  //validateEmplogin Route Begin
  app.route('/validateemplogin/:p_UserId/:p_Password')
    .get(validateemplogin.read_a_validateemplogin);
  //validateEmplogin Route End

  //JobPost Route Begin
  app.route('/JobPost')
  .post(JobPost.create_a_JobPost);
//JobPost Route End
//Fetch Job Begin
  app.route('/Jobs/:p_Location/:p_Flag')
 .get(FetchJob.read_a_FetchJob);
 //Fetch Job End

//EmployerAdminVerification Begin
app.route('/EmployerAdminVerification/:p_RegistraionNumber/:p_RegistrationMonth/:p_RegistrationYear/:p_Emp_Name/:p_Company_Type')
.get(EmployerAdminVerification.read_a_EmployerAdminVerification);
//EmployerAdminVerification End


//DeleteEmpDetail Begin
app.route('/DeleteEmpDetail/:p_Emp_RegId')
.get(EmployerAdminVerification.read_a_DeleteEmpDetail);
//DeleteEmpDetail End

//EmployerRegDetail Begin
app.route('/EmployerRegDetail/:p_Emp_RegId')
    .get(employerregistrations.read_a_EmployerRegDetail);
  //EmployerRegDetail Route End



 
    };

