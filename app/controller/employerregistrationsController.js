'use strict';
var EmployerRegistration = require('../model/employerregistrationsModel.js');

exports.create_a_EmployerRegistration = function(req, res) {
  var new_EmployerRegistration = new EmployerRegistration(req.body);
 
  EmployerRegistration.createEmployerRegistration(new_EmployerRegistration, function(err, EmployerRegistration) {
      if (err)
        res.status(200).send(err);
      else
        res.status(200).send(EmployerRegistration);
    });
};

exports.read_a_EmployerRegistration = function(req, res) {
  var fetch_EmployerRegistration = req.params;
  EmployerRegistration.getEmployerRegistration(req.params, function(err, EmployerRegistration) {
    if (err)
      res.send(err);
    res.json(EmployerRegistration);
  });
};

exports.read_a_EmployerRegDetail = function(req, res) {
  var Fetch_EmployerRegDetail = req.params;
  EmployerRegistration.getEmployerRegDetail(req.params, function(err, EmployerRegDetail) {
    if (err)
      res.send(err);
    res.json(EmployerRegDetail);
  });
};