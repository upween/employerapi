'use strict';
var FetchJob = require('../model/FetchJobModel.js');

exports.read_a_FetchJob = function(req, res) {
  var fetch_job = req.params;
  FetchJob.Fetch_Job(req.params, function(err, FetchJob) {
    if (err)
      res.send(err);
    res.json(FetchJob);
  });
};