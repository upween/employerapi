'use strict';
var ValidateempLogin = require('../model/validateemployerloginModel.js');
import jwt from 'jsonwebtoken';
import config from '../../config.json'

exports.read_a_validateemplogin= function(req, res) {
  var fetch_validateemplogin = req.params;
  ValidateempLogin.getValidateEmpLogin(req.params, function(err, result) {

    if (err)
      res.send(err);
    else {
      let error = false;
      let userDetails = null;
      //console.log(result)
      if (result.length) {
        if (result[0].length) {
          userDetails = result[0][0]
          if (userDetails.ReturnValue == 2) {
            error = true
          }
        }
        else {
          error = true
        }
      }
      else {
        error = true
      }
      if (error) {
        res.status(200).send({
          error: true,
          result: {
            userDetails
          }
        })
      }
      else {
        let tokenObject = JSON.parse(JSON.stringify(userDetails))
        var refreshToken = jwt.sign(tokenObject, config.refreshToken.secret, {
          expiresIn: config.refreshToken.expiresIn
        });
        var token = jwt.sign(tokenObject, config.token.secret, {
          expiresIn: config.token.expiresIn
        });
        res.status(200).send({
          error: false,
          result: {
            userDetails,
            token,
            refreshToken
          }
        })
      }


    }

  });
};
