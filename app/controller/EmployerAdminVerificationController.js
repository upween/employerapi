'use strict';
var EmployerAdminVerification = require('../model/EmployerAdminVerificationModel.js');


exports.read_a_EmployerAdminVerification = function(req, res) {
  var Fetch_Employer_AdminVerification = req.params;
  EmployerAdminVerification.getEmployerAdminVerification(req.params, function(err, EmployerAdminVerification) {
    if (err)
      res.send(err);
    res.json(EmployerAdminVerification);
  });


};

exports.read_a_DeleteEmpDetail = function(req, res) {
    var Delete_Emp_Detail = req.params;
    EmployerAdminVerification.getDeleteEmpDetail(req.params, function(err, DeleteEmpDetail) {
      if (err)
        res.send(err);
      res.json(DeleteEmpDetail);
    });
  
  
  };