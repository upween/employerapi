'use strict';
var Category = require('../model/appModel.js');
/* exports.list_all_category = function(req, res) {
    Category.getAllCategory(function(err, category) {

    console.log('controller')
    if (err)
      res.send(err);
      console.log('res', category);
    res.send(category);
  });
}; Vijay Bhargava */ 
exports.create_a_category = function(req, res) {
  var new_category = new Category(req.body);
  //console.log(new_category,'insert');
  //handles null error 
  //if(!new_category.CategoryName || !new_category.IsActive){
  //  res.status(400).send({ error:true, message: 'Please provide Name/Active' });
  //}
  //else{
    Category.createCategory(new_category, function(err, category) {
      if (err)
        res.send(err);
      res.json(category);
    });
  //}
};
exports.read_a_category = function(req, res) {
  var fetch_category = req.params;
  //console.log(req.params,'select');
  //console.log(req.params.p_categoryid,'select');
  Category.getCategoryById(req.params, function(err, category) {
    if (err)
      res.send(err);
    res.json(category);
  });
};
/* exports.update_a_category = function(req, res) {
    Category.updateById(req.params.categoryId, new Category(req.body), function(err, category) {
    if (err)
      res.send(err);
    res.json(category);
  });
};
exports.delete_a_category = function(req, res) {


    Category.remove( req.params.categoryId, function(err, category) {
    if (err)
      res.send(err);
    res.json({ message: 'Category successfully deleted' });
  });
}; */