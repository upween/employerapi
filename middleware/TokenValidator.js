import jwt from 'jsonwebtoken'
import config from '../config.json'
export default (req,res,next) =>{
    //console.log(req.path.indexOf("/secured/"))
if(req.path.indexOf("/secured/")==-1)
    next()
else{
    let token = req.headers["authorization"]
    jwt.verify(token, config.token.secret, (err, decoded)=> {
      if (err) {
          if(req.headers.refreshtoken){
            jwt.verify(req.headers.refreshtoken,config.refreshToken.secret,(err,decoded)=>{
                if(err)
                     return res.status(401).send({"error": true, "message": 'Failed to authenticate token.' });
                else
                    {   
                        delete decoded["exp"];
                        delete decoded["iat"];
                        var token = jwt.sign(decoded, config.token.secret, {
                            expiresIn:config.token.expiresIn 
                          });
                        res.status(401).send({error:true , data :{token},message:'New token generated'})  
                    }
            })
          }
          else
          return res.status(401).send({"error": true, "message": 'Failed to authenticate token.' });
      }
      else{
          req.body?req.body["p_UserId"] = decoded.CandidateId:null;
        if(req.method == 'GET'){
            if(req.params.p_UserId){
                res.status(401).send("not found")
            }
            req.url=`${req.url}/${decoded.CandidateId}`;
        }
          next()
      }
  });
}
}